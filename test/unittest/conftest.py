from collections.abc import Iterable
from dataclasses import dataclass, field
from itertools import count
from random import uniform
from typing import Sequence, Callable, Generator

from pytest import fixture

from pyploid.genetics.genes.math import FunctionParameterGene, as_math_function
from pyploid.indices.full import FullCytogeneticIndex  # type: ignore
from pyploid.types.algorithms.fitness import Fitness
from pyploid.types.algorithms.reproduce import IndividualFactory
from pyploid.types.cytogenetic_index import TrivialCytogeneticIndex, IndexType
from pyploid.types.gene import Gene
from pyploid.types.individual import Individual, NamedIndividual


@dataclass(frozen=True)
class PointGene(Gene):
    position: FullCytogeneticIndex
    x: float
    y: float


@dataclass(frozen=True)
class PointIndividual(Individual):
    genes: Sequence[PointGene]
    parent_hashes: Sequence[int] = field(default_factory=list)


@fixture
def create_point_individual() -> IndividualFactory[FullCytogeneticIndex]:
    def create(genes: Iterable[Gene[FullCytogeneticIndex]], parents: Iterable[PointIndividual]) -> PointIndividual:
        return PointIndividual(tuple(genes), list(map(id, parents)))

    return create


@fixture
def generate_random_gene_sequence() -> Callable[[int, int, int], Sequence[PointGene]]:
    def _generate(amount_of_genes: int, amount_of_chromosomes: int, amount_of_sets: int) -> Sequence[PointGene]:
        return [
            PointGene(
                FullCytogeneticIndex(
                    gene_number % amount_of_chromosomes,
                    set_number,
                    gene_number // amount_of_chromosomes
                ),
                uniform(-10.0, 10.0),
                uniform(-10.0, 10.0)
            )
            for gene_number in range(amount_of_genes)
            for set_number in range(amount_of_sets)
        ]

    return _generate


@fixture
def random_individual_factory(
        generate_random_gene_sequence: Callable[[int, int, int], Sequence[PointGene]]
) -> Generator[PointIndividual, None, None]:
    return (PointIndividual(generate_random_gene_sequence(10, 2, 2)) for _ in count())


@fixture
def random_individual(random_individual_factory: Generator[PointIndividual, None, None]) -> PointIndividual:
    return next(random_individual_factory)


@fixture
def math_functions() -> Sequence[Callable[[float, float], float]]:
    return [
        lambda x, y: x ** 2 + y ** 2
    ]


@fixture
def random_math_function_genes(
        math_functions: Sequence[Callable[[float, float], float]]
) -> Generator[Sequence[FunctionParameterGene[TrivialCytogeneticIndex]], None, None]:
    return (
        [
            FunctionParameterGene(TrivialCytogeneticIndex(), func, uniform(-10, 10), parameter_position)
            for func in map(as_math_function, math_functions)
            for parameter_position in (0, 1)
            for _ in range(2)
        ]
        for _ in count()
    )


@fixture
def math_function_genes(
        random_math_function_genes: Generator[Sequence[FunctionParameterGene[TrivialCytogeneticIndex]], None, None]
) -> Sequence[FunctionParameterGene[TrivialCytogeneticIndex]]:
    return next(random_math_function_genes)


@fixture
def random_fitness() -> Fitness[NamedIndividual[IndexType]]:
    return lambda _: uniform(0.0, 1.0)



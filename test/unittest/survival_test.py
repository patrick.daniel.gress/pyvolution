from collections.abc import Sequence
from dataclasses import dataclass
from operator import attrgetter
from random import shuffle

from pytest import fixture

from pyploid.algorithms.survival.ratio import top_n_percent_survive
from pyploid.individual.trivial import TrivialPopulation, create_trivial_population
from pyploid.types.cytogenetic_index import TrivialCytogeneticIndex
from pyploid.types.individual import Individual


@dataclass(frozen=True)
class MockIndividual(Individual):
    position: TrivialCytogeneticIndex
    value: int


@fixture
def ordered_population() -> TrivialPopulation[MockIndividual]:
    values: list[int] = list(range(10))
    shuffle(values)
    return create_trivial_population(MockIndividual(TrivialCytogeneticIndex(), i) for i in values)


def mock_fitness(individual: MockIndividual) -> float: return individual.value


def test_top_n_percent_survival(ordered_population: TrivialPopulation[MockIndividual]):
    survivors: Sequence[MockIndividual] = top_n_percent_survive(0.5, mock_fitness, ordered_population)
    deceased: Sequence[MockIndividual] = [member for member in ordered_population.members if member not in survivors]
    min_deceased_fitness: float = min(map(attrgetter('value'), deceased))
    assert len(survivors) == 0.5 * len(ordered_population.members)
    assert all(value <= min_deceased_fitness for value in map(attrgetter('value'), survivors))

def test_top_n_percent_survival_homogenous_fitness(ordered_population: TrivialPopulation[MockIndividual]):
    assert len(top_n_percent_survive(0.5, lambda i: 0.0, ordered_population)) == 0.5*len(ordered_population.members)


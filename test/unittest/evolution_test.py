from collections.abc import Sequence, Callable, Generator
from random import uniform

from toolz import take

from pyploid.algorithms.evolution.simple import create_simple_evolution
from pyploid.genetics.genes.math import create_function_parameter_fitness, FunctionParameterGene, \
    create_value_mutation
from pyploid.genetics.layouts.attribute import dataclass_reindex, create_attribute_based_layout
from pyploid.indices.full import FullCytogeneticIndex
from pyploid.individual.trivial import TrivialIndividual, create_trivial_individual, TrivialPopulation
from pyploid.types.algorithms.evolution import Evolution
from pyploid.types.cytogenetic_index import TrivialCytogeneticIndex
from pyploid.types.layout import GeneLayout


def test_simple_evolution(
        random_math_function_genes: Generator[Sequence[Callable[[float, float], float]], None, None],
):
    fitness = create_function_parameter_fitness()
    evolution: Evolution[TrivialIndividual[FunctionParameterGene]] = create_simple_evolution(
        fitness,
        create_trivial_individual,
        mutation=create_value_mutation(lambda x: x * uniform(0.5, 2))
    )
    layout: GeneLayout[FullCytogeneticIndex, TrivialCytogeneticIndex] = create_attribute_based_layout(
        FunctionParameterGene.get_function,
        FunctionParameterGene.get_parameter_index,
        FullCytogeneticIndex,
        dataclass_reindex
    )
    population: TrivialPopulation[TrivialIndividual[FunctionParameterGene]] = TrivialPopulation(
        [
            TrivialIndividual(
                tuple(layout(genes))
            )
            for genes in take(100, random_math_function_genes)
        ]
    )
    last_best: float = fitness(min(population.members, key=fitness))
    print("Init Population: ", fitness(min(population.members, key=fitness)))
    for i in range(1000):
        population = evolution(population)
        assert len(population.members) == 100
        best: float = fitness(min(population.members, key=fitness))
        print(f"Round {i}: ", best)
        assert best <= last_best
        last_best = min(best, last_best)

    print(population.members)
    top = min(population.members, key=fitness)
    print(top.genes)

from dataclasses import dataclass

from pyploid.genetics.genes.value import ValueGene
from pyploid.types.algorithms.fitness import create_caching_fitness, Fitness
from pyploid.types.cytogenetic_index import SequentialCytogeneticIndex
from pyploid.types.individual import NamedIndividual


@dataclass(frozen=True)
class NumberIndividual(NamedIndividual[ValueGene[SequentialCytogeneticIndex, int]]):
    uuid: str
    genes: list[ValueGene[SequentialCytogeneticIndex, int]]

    def __hash__(self) -> int: return hash(self.uuid)



def test_caching_fitness(random_fitness: Fitness[NamedIndividual[ValueGene[SequentialCytogeneticIndex, int]]]):
    caching_fitness: Fitness[NamedIndividual[ValueGene[SequentialCytogeneticIndex, int]]] = create_caching_fitness(
        random_fitness
    )
    sample_a: NumberIndividual = NumberIndividual('A', [])
    sample_b: NumberIndividual = NumberIndividual('B', [])
    assert caching_fitness(sample_b) != caching_fitness(sample_a)
    assert caching_fitness(sample_a) == caching_fitness(sample_a)
    assert caching_fitness(sample_b) == caching_fitness(sample_b)

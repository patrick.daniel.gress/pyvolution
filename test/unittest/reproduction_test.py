from dataclasses import replace

from toolz import take

from pyploid.algorithms.meiosis.even import create_even_meiosis
from pyploid.types.algorithms.reproduce import create_reproduction


def test_reproduction_mutation(random_individual_factory, create_point_individual):
    def mutate_x_to_23(genes):
        for gene in genes:
            yield replace(gene, x=23)

    reproduce = create_reproduction(create_even_meiosis(), create_point_individual, mutate_x_to_23)
    father, mother = take(2, random_individual_factory)
    child = reproduce([father, mother])
    assert all(
        gene.x == 23 for gene in child.genes
    )
    assert set(child.parent_hashes) == {id(father), id(mother)}
    assert len(child.genes) == len(father.genes)

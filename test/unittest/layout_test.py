from collections.abc import Sequence
from operator import attrgetter

from pyploid.genetics.genes.math import FunctionParameterGene, MathFunction
from pyploid.genetics.layouts.attribute import create_attribute_based_layout, dataclass_reindex
from pyploid.indices.full import FullCytogeneticIndex
from pyploid.types.cytogenetic_index import TrivialCytogeneticIndex
from pyploid.types.gene import Gene
from pyploid.types.layout import GeneLayout


def test_attribute_based_layout(
    math_function_genes: Sequence[FunctionParameterGene[TrivialCytogeneticIndex]]
):
    layout: GeneLayout[FullCytogeneticIndex, TrivialCytogeneticIndex] = create_attribute_based_layout(
        FunctionParameterGene.get_function,
        FunctionParameterGene.get_parameter_index,
        FullCytogeneticIndex,
        dataclass_reindex
    )
    new_layout: Sequence[FunctionParameterGene[FullCytogeneticIndex]] = list(layout(math_function_genes))
    assert len(math_function_genes) == len(new_layout)
    assert len(set(map(attrgetter('position'), new_layout))) == len(math_function_genes)

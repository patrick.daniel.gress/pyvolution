Cytogenetic Indices
===================

In *PyPloid*, gene positions are abstract attributes of a gene that can be expressed by serveral
cytogenetic index types. This approach makes modelling genetic anomalies like trisomy more
natural.

Trivial Cytogenetic Index
-------------------------

.. autoclass:: pyploid.types.cytogenetic_index.TrivialCytogeneticIndex
   :members:
   :show-inheritance:
   :inherited-members:

Sequential Cytogenetic Index
----------------------------

.. autoclass:: pyploid.types.cytogenetic_index.SequentialCytogeneticIndex
   :members:
   :show-inheritance:
   :inherited-members:

Complex Cytogenetic Index
-------------------------

.. autoclass:: pyploid.types.cytogenetic_index.ComplexCytogeneticIndex
   :members:
   :show-inheritance:
   :inherited-members:

Polyploid Cytogenetic Index
---------------------------

.. autoclass:: pyploid.types.cytogenetic_index.PolyploidCytogeneticIndex
   :members:
   :show-inheritance:
   :inherited-members:


Complex Polyploid Cytogenetic Index
-----------------------------------

.. autoclass:: pyploid.types.cytogenetic_index.ComplexPolyploidCytogeneticIndex
   :members:
   :show-inheritance:
   :inherited-members:

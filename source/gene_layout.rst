Gene Layouts
============

A gene layout is a strategy to transform the genome from one cytogenetic index
to another. This usually occurs once when the parameters of the problems are
mapped to genetic space in order to perform evolutionary algorithms.


.. autoclass:: pyploid.types.layout.GeneLayout
   :members:
   :show-inheritance:
   :inherited-members:
   :special-members: __call__


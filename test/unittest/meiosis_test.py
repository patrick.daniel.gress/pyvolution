from collections import Counter
from collections.abc import Callable, Sequence
from operator import itemgetter
from typing import TypeAlias

from pyploid.algorithms.meiosis.even import create_even_meiosis
from pyploid.algorithms.meiosis.polyploid import create_random_pick_from_set_meiosis
from pyploid.types.algorithms.meiosis import Meiosis, predicate_meiosis
from pyploid.types.cytogenetic_index import ComplexPolyploidIndexType
from pyploid.types.gene import Gene
from pyploid.types.individual import Individual

GeneGenerator: TypeAlias = Callable[[int, int, int], Sequence[Gene[ComplexPolyploidIndexType]]]


def test_predicate_meiosis(random_individual: Individual[ComplexPolyploidIndexType]):
    gamette: Sequence[Gene[ComplexPolyploidIndexType]] = predicate_meiosis(
        lambda index: index.set_index == 0, random_individual
    )
    assert len(gamette) == len(random_individual.genes) // 2
    assert all(gene.position.set_index == 0 for gene in gamette)


def test_even_meiosis(
        generate_random_gene_sequence: GeneGenerator,
        random_individual: Individual[ComplexPolyploidIndexType]
):
    homogenous_meiosis: Meiosis = create_even_meiosis()
    gamette: Sequence[Gene[ComplexPolyploidIndexType]] = homogenous_meiosis(random_individual)
    expected_size: int = len(random_individual.genes) // 2
    assert len(gamette) == expected_size
    assert all(
        genes_from_set >= expected_size // 2
        for genes_from_set in Counter(gene.position.set_index for gene in gamette).values()
    )


def test_random_pick_meiosis(
    random_individual: Individual[ComplexPolyploidIndexType]
):
    meiosis: Meiosis[Gene[ComplexPolyploidIndexType]] = create_random_pick_from_set_meiosis()
    gamette: Sequence[Gene[ComplexPolyploidIndexType]] = meiosis(random_individual)
    picked_chromosomes: set[tuple[int, int]] = {
        (gene.position.chromosome_number, gene.position.set_index) for gene in gamette
    }
    assert len(picked_chromosomes) == 2
    assert len(set(map(itemgetter(0), picked_chromosomes))) == 2


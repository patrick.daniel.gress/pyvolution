.. PyPloid documentation master file, created by
   sphinx-quickstart on Tue Feb 13 18:58:45 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to PyPloid's documentation!
===================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   cytogenetic_indices.rst
   gene_layout.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

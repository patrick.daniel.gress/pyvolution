from collections.abc import Generator
from dataclasses import asdict

from pytest import fixture
from toolz import take

from pyploid.indices.full import FullCytogeneticIndex
from pyploid.individual.trivial import TrivialPopulation
from pyploid.recording.types import create_evolution_recorder, RecordRow
from pyploid.types.algorithms.evolution import Evolution
from pyploid.types.algorithms.reproduce import IndividualFactory
from pyploid.types.individual import Population
from test.unittest.conftest import PointIndividual


@fixture
def identity_evolution() -> Evolution[PointIndividual]:
    def _no_evolution(population: Population[PointIndividual]): return population
    return _no_evolution



def test_evolution_recording(
        identity_evolution: Evolution[PointIndividual],
        random_individual_factory: Generator[PointIndividual, None, None]
):
    evolution_iterations: int = 5
    individuals_in_population: int = 6
    records: list[RecordRow] = list()
    recorded_evolution: Evolution[PointIndividual] = create_evolution_recorder(
        identity_evolution, records.append, asdict
    )
    population: Population[PointIndividual] = TrivialPopulation(
        tuple(take(individuals_in_population, random_individual_factory))
    )
    for _ in range(evolution_iterations):
        population = recorded_evolution(population)
    assert len(records) == evolution_iterations + 1
    assert all(len(record['selected_individuals']) == individuals_in_population for record in records)
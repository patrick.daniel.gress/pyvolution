from collections.abc import Callable
from itertools import chain
from typing import Sequence, TypeAlias

from pyploid.algorithms.crossover.index import create_index_base_xover
from pyploid.types.algorithms.crossover import CrossingOver, XoverResult, ChromosomeAssignment
from pyploid.types.cytogenetic_index import ComplexPolyploidIndexType, SequentialCytogeneticIndex
from pyploid.types.gene import Gene

GeneGenerator: TypeAlias = Callable[[int, int, int], Sequence[Gene[ComplexPolyploidIndexType]]]


def test_sequential_xover(generate_random_gene_sequence: GeneGenerator):
    left: Sequence[Gene[ComplexPolyploidIndexType]] = generate_random_gene_sequence(10, 1, 1)
    right: Sequence[Gene[ComplexPolyploidIndexType]] = generate_random_gene_sequence(10, 1, 1)

    xover: CrossingOver[SequentialCytogeneticIndex] = create_index_base_xover()

    alignments: list[XoverResult[SequentialCytogeneticIndex]] = list(xover(left, right))
    assert len(alignments) == len(left) + len(right)
    assert {alignment.gene for alignment in alignments} == set(chain(left, right))

    new_left: Sequence[Gene[SequentialCytogeneticIndex]] = [
        alignment.gene for alignment in alignments if alignment.assignment == ChromosomeAssignment.LEFT
    ]
    new_right: Sequence[Gene[SequentialCytogeneticIndex]] = [
        alignment.gene for alignment in alignments if alignment.assignment == ChromosomeAssignment.RIGHT
    ]
    half_length: int = len(left) // 2
    assert new_left[0:half_length] == right[0:half_length]
    assert new_left[half_length:] == right[half_length:]
    assert new_right[0:half_length] == left[0:half_length]
    assert new_right[half_length:] == left[half_length:]
